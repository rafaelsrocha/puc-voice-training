//
//  VoiceTrainingUITests.swift
//  VoiceTrainingUITests
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import XCTest

class VoiceTrainingUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        //        app.buttons.element(boundBy: 2).tap() // info button
        //        app.buttons.element(boundBy: 3).tap() // example
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTrainingSelection() {
        let app = XCUIApplication()
        app.tables.cells.element(boundBy: 2).tap()
        XCTAssert(app.navigationBars["Treino 2"].otherElements["Treino 2"].exists)
        app.navigationBars["Treino 2"].buttons["Dashboard"].tap()
        app.tables.cells.element(boundBy: 4).tap()
        sleep(1)
        XCTAssert(app.navigationBars["Treino 4"].otherElements["Treino 4"].exists)
    }
    
    func testAudioRecorder() {
        let app = XCUIApplication()
        app.tables.cells.element(boundBy: 3).tap()
        
        let recordButton = app.buttons["RecordButton"]
        let recordsCounterLabel = app.navigationBars["Treino 3"].staticTexts["RecordsCounter"]
        let previousCount = Int(recordsCounterLabel.label)!
        recordButton.press(forDuration: 1.5)
        let currentCount = Int(recordsCounterLabel.label)!
        XCTAssert(currentCount > previousCount)
    }
    
    func testTrainingAttemptsLimit() {
        let app = XCUIApplication()
        app.tables.cells.element(boundBy: 3).tap()
        
        let recordButton = app.buttons["RecordButton"]
        let recordsCounterLabel = app.navigationBars["Treino 3"].staticTexts["RecordsCounter"]
        var counter = Int(recordsCounterLabel.label)!
        while counter < 5 {
            recordButton.press(forDuration: 1.5)
            counter += 1
        }
        recordButton.press(forDuration: 1.5)
        XCTAssert(app.alerts.count > 0)
    }
    
    func testVoiceRecordAnalysis() {
        let app = XCUIApplication()
        app.tables.cells.element(boundBy: 3).tap()
        
        let recordButton = app.buttons["RecordButton"]
        var counter = 0
        
        app.buttons.element(boundBy: 3).tap()
        recordButton.press(forDuration: 3)
        
        while counter < 5 {
            recordButton.press(forDuration: 1.5)
            counter += 1
        }
        app.alerts.buttons.element(boundBy: 0).tap()
        app.tables.cells.element(boundBy: 0).tap()
        
        app.buttons["StartAnalysis"].tap()
        sleep(3)
    }
}
