//
//  VoiceSpectre.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class VoiceSpectre: BaseEntity {
    var spectrePoints: [CGPoint]!
    
    init(spectrePoints: [CGPoint]) {
        super.init()
        self.spectrePoints = spectrePoints
    }
    
    func analyze() {
        
    }
}
