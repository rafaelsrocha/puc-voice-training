//
//  VoiceRecord.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class VoiceRecord: BaseEntity {
    var path: String?
    var voiceSpectre: VoiceSpectre?
    
    func getVoiceRecord() -> URL {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let urlPath = URL(string: paths.first!)!.appendingPathComponent(path!)
        return urlPath
    }
    
    func play() {
        
    }
    
    func delete() {
        
    }
    
    func generateSpectre() {
        
    }
    
    func analyzeSpectre() {
        
    }
}
