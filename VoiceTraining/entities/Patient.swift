//
//  Patient.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class Patient: BaseEntity {
    var name: String?
    var age: Int?
    var cpf: String?
    var trainings: [Training]?
}
