//
//  Training.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class Training: BaseEntity {
    var name: String?
    var trainingDescription: String?
    var observation: String?
    var orderedPosition: Int?
    var done: NSNumber?
    var doctorAudioPath: String?
    var voiceRecords: [VoiceRecord]?
    
    static var maxRecordsCount: Int {
        return 5
    }
    
    func getDoctorExample() -> URL {
        return Bundle.main.url(forResource: "example", withExtension: "m4a")!
    }
    
    func analyzeSpectre() {
        
    }
    
    func record() -> URL {
        let trainingKey = "training_\(dbId!)"
        var name: String = "\(trainingKey)-record_1.m4a"
        var records: [String] = []
        
        let voiceRecord = VoiceRecord()
        voiceRecord.dbId = "1"
        
        if let trainingRecords = UserDefaults.standard.stringArray(forKey: trainingKey) {
            name = "\(trainingKey)-record_\(trainingRecords.count + 1).m4a"
            records = trainingRecords
            voiceRecord.dbId = "\(trainingRecords.count + 1)"
        }
        
        voiceRecord.path = name
        voiceRecords?.append(voiceRecord)
        
        records.append(name)
        UserDefaults.standard.set(records, forKey: trainingKey)
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let pathUrl = URL(string: paths.first!)!.appendingPathComponent(name)
        return pathUrl
    }
    
    func saveRecord() {
        
    }
    
    func deleteRecord(_ record: VoiceRecord) {
        let trainingKey = "training_\(dbId!)"
        if let trainingRecords = UserDefaults.standard.stringArray(forKey: trainingKey) {
            var newArray = trainingRecords
            let name = "\(trainingKey)-record_\(record.dbId!).m4a"
            newArray.remove(at: newArray.index(of: name)!)
            UserDefaults.standard.set(newArray, forKey: trainingKey)
            
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let pathUrl = URL(string: paths.first!)!.appendingPathComponent(name)
            try! FileManager.default.removeItem(atPath: pathUrl.absoluteString)
        }
    }
    
    func openRecords() {
        
    }
    
    func getRecord() {
        
    }
    
    func playRecord() {
        
    }
}
