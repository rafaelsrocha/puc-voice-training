//
//  BaseEntity.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class BaseEntity: NSObject {
    var dbId: String!
}
