//
//  TrainingAnalysisViewController.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/11/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit
import AudioKit
import AudioKitUI
import Charts

class TrainingAnalysisViewController: BaseViewController, ChartViewDelegate {
    
    @IBOutlet weak var audioPlotContainer: UIView!
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var startButton: UIButton!
    
    var record: VoiceRecord!
    
    private var player: AKAudioPlayer!
    private var tracker: AKFrequencyTracker!
    private var timer: Timer?
    
    var chartData: LineChartData!
    
    private var fftLineChartEntry = [ChartDataEntry]()
    private var highestDotsChartEntry = [ChartDataEntry]()
    private var lowestDotsChartEntry = [ChartDataEntry]()
    private var fftLine: LineChartDataSet {
        let fftLine = LineChartDataSet(values: self.fftLineChartEntry, label: "AMP")
        fftLine.colors = [NSUIColor.green]
        fftLine.drawCirclesEnabled = false
        return fftLine
    }
    
    private var highestDotsLine: LineChartDataSet {
        let highestDotsLine = LineChartDataSet(values: self.highestDotsChartEntry , label: nil)
        highestDotsLine.colors = [NSUIColor.clear]
        highestDotsLine.drawCirclesEnabled = true
        highestDotsLine.circleColors = [NSUIColor.red]
        return highestDotsLine
    }
    
    private var lowestDotsLine: LineChartDataSet {
        let lowestDotsLine = LineChartDataSet(values: self.lowestDotsChartEntry , label: nil)
        lowestDotsLine.colors = [NSUIColor.clear]
        lowestDotsLine.drawCirclesEnabled = true
        lowestDotsLine.circleColors = [NSUIColor.blue]
        return lowestDotsLine
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Gravação \(record.dbId!)"

        let file = try! AKAudioFile(forReading: record.getVoiceRecord())
        player = try! AKAudioPlayer(file: file)
        player.volume = 10
        
        tracker = AKFrequencyTracker(player)
        chartData = LineChartData()
        
        let outputPlot = AKNodeFFTPlot(player, frame: audioPlotContainer.bounds)
        outputPlot.plotType = .rolling
        outputPlot.shouldFill = true
        outputPlot.shouldMirror = true
        outputPlot.setRollingHistoryLength(200)
        outputPlot.color = .white
        outputPlot.backgroundColor = .clear
        audioPlotContainer.addSubview(outputPlot)
        
        AudioKit.output = tracker
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startButton.layer.cornerRadius = 20
    }
    
    @IBAction func startAnalysis(_ sender: Any) {
        startButton.isHidden = true
        try! AudioKit.start()
        player.play()
        
        let fft = AKFFTTap(tracker)
        timer = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true) { (timer) in
            if let max = fft.fftData.max() {
//                let index = fft.fftData.index(of: max)
//                let value = ChartDataEntry(x: Double(self.fftLineChartEntry.count), y: max)
                let value = ChartDataEntry(x: Double(self.fftLineChartEntry.count), y: self.tracker.amplitude)
                self.fftLineChartEntry.append(value)
                self.reloadChart()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + player.duration) {
            self.timer?.invalidate()
            self.analyzeChart()
            self.player.stop()
            try! AudioKit.stop()
        }
    }
    
    func reloadChart() {
        for data in chartData.dataSets {
            chartData.removeDataSet(data)
        }
        
        chartData.addDataSet(fftLine)
        chart.notifyDataSetChanged()
        chart.data = chartData
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.axisMinimum = 0
        chart.leftAxis.axisMinimum = 0
        chart.rightAxis.enabled = false
        chart.chartDescription?.enabled = false
    }
    
    func analyzeChart() {
        let sorted = fftLineChartEntry.sorted { (entry1, entry2) -> Bool in
            return entry1.y > entry2.y
        }
        
        highestDotsChartEntry.append(sorted.first!)

        // Highest
        for entry in sorted {
            if highestDotsChartEntry.count == 2 {
                break
            }
            var hasPoint = false
            for dot in highestDotsChartEntry {
                if entry.y == dot.y {
                    hasPoint = true
                }
            }
            if !hasPoint {
                highestDotsChartEntry.append(entry)
            }
        }
        
        // Lowest
        let lowSort = sorted.reversed()
        for entry in lowSort {
            if lowestDotsChartEntry.count == 5 {
                break
            }
            let index = fftLineChartEntry.index(of: entry)!
            if index - 10 < 0 { continue }
            if index + 10 > fftLineChartEntry.count - 1 { continue }
            var hasPoint = false
            for dot in lowestDotsChartEntry {
                if entry.y == dot.y {
                    hasPoint = true
                }
            }
            if hasPoint { continue }
            var leftValid = false
            for i in index - 10...index {
                let compared = fftLineChartEntry[i]
                if compared.y >= entry.y + 0.04 {
                    leftValid = true
                }
            }
            var rightValid = false
            for i in index...index + 10 {
                let compared = fftLineChartEntry[i]
                if compared.y >= entry.y + 0.04 {
                    rightValid = true
                }
            }
            if leftValid && rightValid {
                lowestDotsChartEntry.append(entry)
            }
        }
        
        chartData.addDataSet(highestDotsLine)
        chartData.addDataSet(lowestDotsLine)
        chart.data = chartData
    }
}
