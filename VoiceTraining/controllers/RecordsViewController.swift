//
//  RecordsViewController.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/11/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class RecordsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var training: Training!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Gravações"
        tableView.tableFooterView = UIView()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return training.voiceRecords!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        cell.textLabel?.text = "Gravação \(indexPath.row + 1)"
        cell.textLabel?.textColor = UIColor(red: 62/255, green: 62/255, blue: 62/255, alpha: 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let record = training.voiceRecords![indexPath.row]
        performSegue(withIdentifier: "ShowTrainingAnalysis", sender: record)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Aviso", message: "Você realmente deseja excluir essa gravação?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Sim", style: .default) { (action) in
                let record = self.training.voiceRecords![indexPath.row]
                self.training.deleteRecord(record)
                self.training.voiceRecords!.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
            present(alert, animated: true, completion: nil)
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowTrainingAnalysis" {
            let analysisController = segue.destination as! TrainingAnalysisViewController
            analysisController.record = sender as! VoiceRecord
        }
    }
}
