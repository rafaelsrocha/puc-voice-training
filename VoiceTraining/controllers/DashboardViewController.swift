//
//  DashboardViewController.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let profileRightView = Bundle.main.loadNibNamed("DashboardProfileView", owner: self, options: nil)!.first as! DashboardProfileView
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: profileRightView)
        
        tableView.register(UINib(nibName: "DashboardOverviewCell", bundle: Bundle.main), forCellReuseIdentifier: "DashboardOverviewCell")
        tableView.register(UINib(nibName: "DashboardTrainingCell", bundle: Bundle.main), forCellReuseIdentifier: "DashboardTrainingCell")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.defaultTrainings.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "DashboardOverviewCell") as! DashboardOverviewCell
        }
        let training = appDelegate.defaultTrainings[indexPath.row - 1]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardTrainingCell") as! DashboardTrainingCell
        cell.label.text = training.name
        if indexPath.row == 1 {
            cell.setStatus()
        } else if indexPath.row == 2 {
            cell.setStatus("failure")
        } else {
            cell.setStatus("success")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 380
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let training = appDelegate.defaultTrainings[indexPath.row - 1]
        performSegue(withIdentifier: "ShowTraining", sender: training)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowTraining" {
            let trainingController = segue.destination as! TrainingViewController
            trainingController.training = sender as! Training
        }
    }
}
