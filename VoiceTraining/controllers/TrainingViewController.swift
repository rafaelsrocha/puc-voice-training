//
//  TrainingViewController.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/10/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit
import AudioKit
import AudioKitUI
import PopupDialog

class TrainingViewController: BaseViewController, EZMicrophoneDelegate, EZRecorderDelegate, EZAudioPlayerDelegate {

    @IBOutlet weak var exampleContainer: UIView!
    @IBOutlet weak var examplePlotHolder: UIView!
    @IBOutlet weak var exampleTracker: UIProgressView!
    @IBOutlet weak var recorderPlot: EZAudioPlotGL!
    @IBOutlet weak var recordButton: UIButton!
    
    var training: Training!
    
    private var trainingListView: TrainingRightButtonView!
    private var recordingSession: AVAudioSession!
    private var examplePlayer: AKAudioPlayer!
    private var examplePlot: AKNodeOutputPlot!
    private var exampleTimer: Timer!
    private var recorder: EZRecorder!
    private var microphone: EZMicrophone!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = training.name
        trainingListView = Bundle.main.loadNibNamed("TrainingRightButtonView", owner: self, options: nil)!.first as! TrainingRightButtonView
        trainingListView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openRecordsList)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: trainingListView)

        recordButton.layer.borderColor = UIColor.white.cgColor
        recordButton.layer.borderWidth = 2
        recordButton.layer.cornerRadius = recordButton.frame.width / 2
        recordButton.layer.masksToBounds = true
        
        recorderPlot.plotType = .rolling
        
        let file = try! AKAudioFile(forReading: training.getDoctorExample())
        examplePlayer = try! AKAudioPlayer(file: file)
        examplePlayer.volume = 10
        
        examplePlot = AKNodeOutputPlot(examplePlayer, frame: examplePlotHolder.bounds)
        examplePlot.backgroundColor = .clear
        examplePlot.plotType = .rolling
        examplePlot.shouldFill = true
        examplePlot.shouldMirror = true
        examplePlot.color = UIColor(red: 253/255, green: 177/255, blue: 96/255, alpha: 1)
        examplePlot.plotType = .rolling
        examplePlotHolder.addSubview(examplePlot)
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(recorderLongPress(_:)))
        recordButton.addGestureRecognizer(longGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trainingListView.count = training.voiceRecords!.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] granted in
                DispatchQueue.main.async {
                    if granted {
                        print("granted")
                        self.microphone = EZMicrophone(delegate: self)
                    } else {
                        self.showFailureAlert()
                    }
                }
            }
        } catch {
            showFailureAlert()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        exampleTracker.progress = 0
        examplePlot.clear()
        recorderPlot.clear()
    }
    
    func showFailureAlert() {
        let alert = UIAlertController(title: "Erro", message: "Algo deu errado ao garantir a permissão de gravação.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func recorderLongPress(_ sender: UIGestureRecognizer) {
        switch sender.state {
        case .began:
            guard training.voiceRecords!.count < Training.maxRecordsCount else {
                let alert = UIAlertController(title: "Ops", message: "Você excedeu o número máximo de gravações por treino.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.6){
                        self.performSegue(withIdentifier: "ShowRecords", sender: nil)
                    }
                }
                return
            }
            
            recorderPlot.clear()
            recorder = EZRecorder(url: training.record(),
                                  clientFormat: microphone.audioStreamBasicDescription(),
                                  fileType: .M4A,
                                  delegate: self)

            microphone.startFetchingAudio()
            break
        case .ended:
            guard training.voiceRecords!.count < Training.maxRecordsCount else { return }
            microphone.stopFetchingAudio()
            recorder.closeAudioFile()
            trainingListView.shake()
            trainingListView.count = training.voiceRecords!.count
            break
        default:
            break
        }
    }
    
    // MARK: - EZMicrophoneDelegate
    
    func microphone(_ microphone: EZMicrophone!,
                    hasAudioReceived buffer: UnsafeMutablePointer<UnsafeMutablePointer<Float>?>!,
                    withBufferSize bufferSize: UInt32,
                    withNumberOfChannels numberOfChannels: UInt32) {
        DispatchQueue.main.async { [weak self] in
            self?.recorderPlot.updateBuffer(buffer[0], withBufferSize: bufferSize)
        }
    }
    func microphone(_ microphone: EZMicrophone!,
                    hasBufferList bufferList: UnsafeMutablePointer<AudioBufferList>!,
                    withBufferSize bufferSize: UInt32,
                    withNumberOfChannels numberOfChannels: UInt32) {
        recorder.appendData(from: bufferList, withBufferSize: bufferSize)
    }
    
    @IBAction func openExampleInfo(_ sender: Any) {
        let title = "OBSERVAÇÕES SOBRE O TREINO"
        let message = "Aqui poderá mostrar observações que o profissional de saúde achar necessárias sobre o treino em questão."

        let popup = PopupDialog(title: title, message: message)
        popup.addButton(DefaultButton(title: "OK", action: nil))
        popup.transitionStyle = .bounceDown
        
        present(popup, animated: true, completion: nil)
    }
    
    @IBAction func playExample(_ sender: Any) {
        exampleTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateExampleProgress), userInfo: nil, repeats: true)
        examplePlot.clear()
        examplePlot.setRollingHistoryLength(200)
        AudioKit.output = examplePlayer
        
        try! AudioKit.start()
        examplePlayer.play()
        examplePlayer.completionHandler = {
            self.exampleTracker.progress = 1
            self.exampleTimer.invalidate()
            try! AudioKit.stop()
            self.examplePlayer.stop()
        }
    }
    
    @objc func updateExampleProgress() {
        exampleTracker.progress = Float(examplePlayer.currentTime / examplePlayer.duration)
    }
    
    @objc func openRecordsList() {
        guard training.voiceRecords!.count > 0 else { return }
        performSegue(withIdentifier: "ShowRecords", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowRecords" {
            let recordsController = segue.destination as! RecordsViewController
            recordsController.training = training
        }
    }
}
