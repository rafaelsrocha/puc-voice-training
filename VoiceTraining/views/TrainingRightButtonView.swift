//
//  TrainingRightButtonView.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/12/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class TrainingRightButtonView: UIView {

    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        counterView.layer.cornerRadius = counterView.frame.width / 2
        counterView.clipsToBounds = true
    }
    
    var count: Int = 0 {
        didSet {
            counterView.isHidden = count == 0
            counterLabel.isHidden = count == 0
            counterLabel.text = "\(count)"
        }
    }
    
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
}
