//
//  DashboardProfileView.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/12/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class DashboardProfileView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
    }
}
