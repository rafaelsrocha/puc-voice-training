//
//  DashboardTrainingCell.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit

class DashboardTrainingCell: UITableViewCell {

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        indicatorView.layer.borderWidth = 3
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        indicatorView.layer.cornerRadius = indicatorView.frame.width / 2
        indicatorView.clipsToBounds = true
    }

    func setStatus(_ status: String? = "") {
        switch status! {
        case "failure":
            indicatorView.layer.borderColor = UIColor(red: 252/255, green: 33/255, blue: 55/255, alpha: 1).cgColor
            break
        case "success":
            indicatorView.layer.borderColor = UIColor(red: 86/255, green: 209/255, blue: 194/255, alpha: 1).cgColor
            break
        default:
            indicatorView.layer.borderColor = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1).cgColor
            break
        }
    }
}
