//
//  DashboardOverviewCell.swift
//  VoiceTraining
//
//  Created by Rafael Rocha on 6/9/18.
//  Copyright © 2018 Rafael Rocha. All rights reserved.
//

import UIKit
import UICircularProgressRing

class DashboardOverviewCell: UITableViewCell {

    @IBOutlet weak var circularProgressRing: UICircularProgressRingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        circularProgressRing.font = UIFont.systemFont(ofSize: 70)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
